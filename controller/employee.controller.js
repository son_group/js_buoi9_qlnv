function getEmployeeInfoFromForm(){
    var employeeID = document.getElementById("tknv").value;
    var employeeName = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var workDay = document.getElementById("datepicker").value;
    var basicSalary = document.getElementById("luongCB").value;
    var position = document.getElementById("chucvu").value;
    var workHourPerMonth = document.getElementById("gioLam").value;

    return new Employee(employeeID,employeeName,email,password,workDay,basicSalary,position,workHourPerMonth);
}

function renderEmployee(employeeList){
    var contentHTML = "";
    for(var index=0;index<employeeList.length;index++){
        var employee = employeeList[index];
        var position = "";
        if(employee.position==3){
            position = "Giám đốc";
        }else if(employee.position ==2){
            position = "Trưởng phòng";
        }else {
            position = "Nhân viên";
        }
        // console.log("studentID of render",student);
        var rowHTML = `<tr>
            <td>${employee.employeeID}</td>
            <td>${employee.employeeName}</td>
            <td>${employee.email}</td>
            <td>${employee.workDay}</td>
            <td>${position}</td>
            <td>${employee.totalSalary()}</td>
            <td>${employee.typeEmployee()}</td>
            <td>
                <button onclick = "deleteEmployee('${employee.employeeID}')" class="btn btn-danger">Xoá</button>
                <button onclick ="editEmployee('${employee.employeeID}')" class="btn btn-warning" data-toggle="modal"
                data-target="#myModal">Sửa</button>
            </td>
            </tr>
        `;        
        contentHTML += rowHTML;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function findIndex(employeeID, employeeList){
    // console.log("studentID",studentID);
    for (var index = 0; index<employeeList.length; index++){
        var employee = employeeList[index];
        
        if(employeeID == employee.employeeID){
            // console.log("gia tri index trong find Index ",index);
            return index;
        }
    }
    return -1;
}

function showEmployeeToForm(employee){
    document.getElementById("tknv").disabled = true;
    document.getElementById("tknv").value = employee.employeeID;
    document.getElementById("name").value = employee.employeeName;
    document.getElementById("email").value = employee.email;
    document.getElementById("password").value = employee.password;
    document.getElementById("datepicker").value = employee.workDay;
    document.getElementById("luongCB").value = employee.basicSalary;
    document.getElementById("chucvu").value = employee.position;
    document.getElementById("gioLam").value = employee.workHourPerMonth;
    document.getElementById("btnThemNV").disabled = true;
}

function clearForm(){
    document.querySelectorAll(".sp-thongbao").forEach(spanTag=>spanTag.style.display = "none");
    // document.querySelectorAll(".sp-thongbao").forEach(spanTag=>spanTag.value = "");
}