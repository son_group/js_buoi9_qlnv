var employeeList = [];
console.log("Có chạy JS");

function addEmployee() {
  console.log("Có chạy vô đây");
  var employee = getEmployeeInfoFromForm();
  var isValid = checkValid(employee);
  // var isUnique = checkDuplicateID.checkUnique(employee.employeeID,employeeList);

  if (isValid) {
    employeeList.push(employee);
    renderEmployee(employeeList);
  }

  // document.getElementById("tbTKNV").style.display= "block";
  // document.getElementById("tbTKNV").innerHTML = "Tai sao khong show ra";
}

function checkValid(employee) {
  var isValid =
    validation.checkEmptyInput(
      employee.employeeID,
      "tbTKNV",
      "Tài khoản không được để trống"
    ) &&
    validation.checkLength(
      employee.employeeID,
      "tbTKNV",
      "Tài khoản phải từ 4 đến 6 kí tự",
      4,
      6
    ) &&
    checkDuplicateID.checkUnique(employee.employeeID, employeeList);
  console.log("valid tai khoan ", isValid);

  isValid =
    isValid &
    (validation.checkEmptyInput(
      employee.employeeName,
      "tbTen",
      "Tên không được để trống"
    ) &&
      validation.checkLetter(
        employee.employeeName,
        "tbTen",
        "Tên chỉ bao gồm kí tự chữ"
      ));

  console.log("valid name ", isValid);

  isValid =
    isValid &
    (validation.checkEmptyInput(
      employee.email,
      "tbEmail",
      "Email không được để trống"
    ) &&
      validation.checkEmailValid(
        employee.email,
        "tbEmail",
        "Email không hợp lệ"
      ));

  console.log("valid email ", isValid);
  isValid =
    isValid &
    (validation.checkEmptyInput(
      employee.password,
      "tbMatKhau",
      "Mật khẩu không được để trống"
    ) &&
      validation.checkPassword(
        employee.password,
        "tbMatKhau",
        "Mật khẩu từ 6-10 kí tự, ít nhất 1 số, 1 chữ in hoa và 1 kí tự đặc biệt"
      ));

  console.log("valid mk", isValid);

  isValid =
    isValid &
    (validation.checkEmptyInput(
      employee.workDay,
      "tbNgay",
      "Ngày làm không được để trống"
    ) &&
      validation.checkDateFormat(
        employee.workDay,
        "tbNgay",
        "Ngày làm phải có định dạng mm/dd/yyyy"
      ));

  console.log("valid ngay lam viec ", isValid);

  isValid =
    isValid &
    (validation.checkEmptyInput(
      employee.basicSalary,
      "tbLuongCB",
      "Lương cơ bản không được để trống"
    ) &&
      validation.checkSalaryRange(
        employee.basicSalary,
        "tbLuongCB",
        "Lương cơ bản trong khoảng 1.000.000 đến 20.000.000",
        1000000,
        20000000
      ));

  console.log("valid luong co ban", isValid);

  isValid =
    isValid &
    (validation.checkEmptyInput(
      employee.position,
      "tbChucVu",
      "Chức vụ không được để trống"
    ) &&
      validation.checkPositionValid(
        employee.position,
        "tbChucVu",
        "Phải chọn một trong các chức vụ: Giám đốc, Trưởng phòng, Nhân viên"
      ));

  console.log("valid chuc vu ", isValid);

  isValid =
    isValid &
    (validation.checkEmptyInput(
      employee.workHourPerMonth,
      "tbGiolam",
      "Giờ làm không được để trống"
    ) &&
      validation.checkWorkHours(
        employee.workHourPerMonth,
        "tbGiolam",
        "Giờ làm chỉ trong khoảng từ 80 - 200 giờ",
        80,
        200
      ));

  console.log("valid hour ", isValid);

  return isValid;
}

function editEmployee(employeeID) {
  var index = findIndex(employeeID, employeeList);
  if (index != -1) {
    var employee = employeeList[index];
    showEmployeeToForm(employee);
  }
}

function deleteEmployee(employeeID) {
  var index = findIndex(employeeID, employeeList);
  if (index != -1) {
    employeeList.splice(index, 1);
    renderEmployee(employeeList);
  }
}

function updateEmployee() {
  var employeeID = document.getElementById("tknv").value;
  var index = findIndex(employeeID, employeeList);
  if (index != -1) {
    var employee = getEmployeeInfoFromForm();
    employeeList[index] = employee;
    renderEmployee(employeeList);
    clearForm();
  }
}

function closeForm() {
  clearForm();
}

function searchEmployeeType() {
  console.log("Bam nut search");

  var employeeType = document.getElementById("searchName").value;
  var mintime = -1;
  var maxtime = -1
  console.log("text inputted: ", employeeType);
  switch (employeeType.toLowerCase()) {
    case "xuất sắc":
      mintime = 192;
      maxtime = 201;
      break;
    case "giỏi":
      mintime = 167;
      maxtime = 192;
      break;
    case "khá":
      mintime = 160;
      maxtime = 167
    case "trung bình":
      mintime = 0;
      maxtime = 160;
      break;
    default:
      // nothing to do
  }
  var employeeListSearch = [];
  if (maxtime =-1){
    document.getElementById("tableDanhSach").innerHTML = `Không có xếp loại như bạn nhập`;

  }else{
    employeeList.forEach(employee => {
      if(employee.workHourPerMonth>=mintime && employee.workHourPerMonth<maxtime){
        employeeListSearch.push(employee);
      }
    });
    renderEmployee(employeeListSearch);
  }
}
