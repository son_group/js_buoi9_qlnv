function Employee(employeeID,employeeName,email,password,workDay,basicSalary,position,workHourPerMonth){
    this.employeeID = employeeID;
    this.employeeName = employeeName;
    this.email = email;
    this.password = password;
    this.workDay = workDay;
    this.basicSalary = basicSalary;
    this.position = position;
    this.workHourPerMonth = workHourPerMonth;

    this.totalSalary = function(){
        return this.position*this.basicSalary;
    }

    this.typeEmployee = function(){
        var type = "";
        if(this.workHourPerMonth>=192){
            type = "Xuất Sắc";
        }else if(this.workHourPerMonth>=176){
            type = "Giỏi";
        }else if(this.workHourPerMonth>=160){
            type = "Khá";
        }else{
            type = "Trung bình";
        }
        return type;
    }
}