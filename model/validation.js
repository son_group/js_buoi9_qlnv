var validation = {
    checkEmptyInput: function(value,idError,message){
        if(value.length == 0){   
            document.getElementById(idError).style.display = "block";            
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    checkLength: function(value, idError, message, minlength, maxlength){
        if(value.length <minlength || value.length>maxlength){
            document.getElementById(idError).style.display = "block";     
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    checkLetter: function(value,idError,message){
        const letters = /^[A-Za-z]+$/;
        if(value.match(letters)){
            document.getElementById(idError).innerText = "";
            return true;
        }else{
            document.getElementById(idError).style.display = "block";     
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
    checkEmailValid: function(value,idError,message){
        const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(value.toLowerCase().match(re)){
            document.getElementById(idError).innerText = "";
            return true;
        }else{
            document.getElementById(idError).style.display = "block";     
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
    checkPassword: function(value,idError,message){
        // const re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{6,8}$/;
        const re = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*]).{6,10}$/;
        if(value.match(re)){
            document.getElementById(idError).innerText = "";
            return true;
        }else{
            document.getElementById(idError).style.display = "block";     
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
    checkDateFormat: function(value,idError,message){
        const date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
        if(value.match(date_regex)){
            document.getElementById(idError).innerText = "";
            return true;
        }else{
            document.getElementById(idError).style.display = "block";     
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
    checkSalaryRange: function(value,idError,message,minSalary,maxSalary){
        if(value<minSalary || value > maxSalary){
            document.getElementById(idError).style.display = "block";     
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    checkPositionValid: function(value,idError,message){
        if(value == 0){
            document.getElementById(idError).style.display = "block";     
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    checkWorkHours: function(value,idError,message,minHour,maxHour){
        if(value<minHour || value > maxHour){
            document.getElementById(idError).style.display = "block";     
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    }

}

var checkDuplicateID = {
    checkUnique: function(employeeID, employeeList){
        var index = -1;
        var i = 0;
        while(i<employeeList.length && index ==-1){
            if(employeeID == employeeList[i].employeeID){
                index = i;
            }
            i++;
        }
        
        if(index==-1){            
            document.getElementById("tbTKNV").innerText = "";
            return true
        }else{            
            document.getElementById("tbTKNV").style.display = "block"; 
            document.getElementById("tbTKNV").innerText = "Tài khoản bị trùng";
            return false;
        }
        
    }
}

